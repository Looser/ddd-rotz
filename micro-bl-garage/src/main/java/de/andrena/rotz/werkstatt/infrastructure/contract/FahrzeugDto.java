package de.andrena.rotz.werkstatt.infrastructure.contract;

import de.andrena.rotz.werkstatt.domain.Kopfdaten;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FahrzeugDto extends Kopfdaten {
    private String fahrzeugnummer;
    private String kennzeichen;
    private String fahrzeugklasse;
    private Leasing leasing;
    private String station;
}
