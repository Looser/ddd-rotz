package de.andrena.rotz.werkstatt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Data
@MappedSuperclass
public abstract class Kopfdaten implements Serializable {

    private static final long serialVersionUID = 5868433974616798422L;

    @Id
    @GeneratedValue
    private Integer id;

    @Version
    private int version;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created;

    public Kopfdaten() {
        super();
        created = new Date();
    }
}
