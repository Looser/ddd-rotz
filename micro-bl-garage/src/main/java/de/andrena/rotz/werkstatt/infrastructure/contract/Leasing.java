package de.andrena.rotz.werkstatt.infrastructure.contract;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Leasing {
    private String leasingBeginn;
    private String leasingEnde;
}
