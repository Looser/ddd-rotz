package de.andrena.rotz.werkstatt.domain;

import javax.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class ReparaturenTODOs {

  private String todos = "";

  public void add(String todo) {
    todos += todo;
  }
}
