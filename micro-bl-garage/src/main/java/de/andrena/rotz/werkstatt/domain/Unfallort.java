package de.andrena.rotz.werkstatt.domain;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Unfallort {

  private double gpsLongitude;
  private double gpdLatitude;

}
