package de.andrena.rotz.werkstatt.infrastructure.service;

import de.andrena.rotz.werkstatt.infrastructure.contract.FahrzeugDto;
import de.andrena.rotz.werkstatt.infrastructure.contract.ReparaturAuftrag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

@Slf4j
@Service
public class Auftragseingang {

    @Autowired
    RestTemplate restTemplate;

    @JmsListener(destination = "reparaturAuftrag", containerFactory = "myFactory")
    public void reparaturAuftragEmpfangen(ObjectMessage reparaturAuftrag) throws JMSException {
        log.info("receivedMessage:" + reparaturAuftrag);
        log.info("receivedMessage: " + reparaturAuftrag.getBody(ReparaturAuftrag.class));
        //Fahrzeug fahrzeug = new Fahrzeug(reparaturAuftrag.getKennzeichen(), Date.from(Instant.now()), FahrzeugTyp.AUTO);

        //ReparaturenTODOs reparaturenTODOs = new ReparaturenTODOs();
        //reparaturenTODOs.add(reparaturAuftrag.getReparaturTodos());
        //fahrzeug.reparaturVereinbaren(reparaturenTODOs);

        FahrzeugDto fahrzeugDto = restTemplate.getForObject("http://10.132.1.60:9095/api/fahrzeuge/1", FahrzeugDto.class);
        log.info(fahrzeugDto.toString());
    }
}
