package de.andrena.rotz.werkstatt.infrastructure.contract;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReparaturAuftrag implements Serializable {

    private static final long serialVersionUID = 1L;

    private String kennzeichen;
    private String reparaturTodos;
}
