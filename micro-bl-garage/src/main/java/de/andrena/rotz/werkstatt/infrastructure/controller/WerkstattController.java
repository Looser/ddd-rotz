package de.andrena.rotz.werkstatt.infrastructure.controller;

import de.andrena.rotz.werkstatt.domain.Auftragsbuch;
import de.andrena.rotz.werkstatt.domain.Fahrzeug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class WerkstattController {

    @Autowired
    Auftragsbuch auftragsbuch;

    @Autowired
    Tracer tracer;

    @GetMapping("/auftragsbuch")
    public List<Fahrzeug> zeigeAuftragsbuchAn() {
        Span currentSpan = tracer.currentSpan();
        currentSpan.tag("message", "nachricht");
        return auftragsbuch.anzeige();
    }
}
