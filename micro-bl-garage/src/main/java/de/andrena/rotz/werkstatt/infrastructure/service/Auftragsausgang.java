package de.andrena.rotz.werkstatt.infrastructure.service;

import lombok.AllArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class Auftragsausgang {

    private JmsTemplate jmsTemplate;

    public void abgeschlosseneReparaturMelden(String kennzeichen) {
        jmsTemplate.setDefaultDestinationName("reparaturAbschluss");
        jmsTemplate.send(session -> session.createObjectMessage(kennzeichen));
    }

    public void rechnungSenden(String betrag) {
        jmsTemplate.setDefaultDestinationName("rechnung");
        jmsTemplate.send(session -> session.createObjectMessage(betrag));
    }
}
