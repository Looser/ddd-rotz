package de.andrena.rotz.werkstatt;

import de.andrena.rotz.werkstatt.domain.Auftragsbuch;
import de.andrena.rotz.werkstatt.domain.Fahrzeug;
import de.andrena.rotz.werkstatt.domain.ReparaturenTODOs;
import de.andrena.rotz.werkstatt.domain.Unfallort;
import de.andrena.rotz.werkstatt.infrastructure.contract.FahrzeugDto;
import de.andrena.rotz.werkstatt.infrastructure.repository.AuftragsbuchRepository;
import de.andrena.rotz.werkstatt.infrastructure.service.Auftragsausgang;
import de.andrena.rotz.werkstatt.infrastructure.service.Auftragseingang;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static de.andrena.rotz.werkstatt.domain.FahrzeugTyp.AUTO;
import static de.andrena.rotz.werkstatt.domain.FahrzeugTyp.HEISSLUFTBALLON;
import static java.time.Instant.now;

@Slf4j
@EnableJms
@SpringBootApplication
public class Werkstatt {

    public static Auftragsbuch auftragsbuch;
    public static Auftragseingang auftragseingang;
    public static Auftragsausgang auftragsausgang;

    public static void main(String[] args) {
        SpringApplication.run(Werkstatt.class, args);
    }

    @Bean
    public ApplicationRunner testAPI(AuftragsbuchRepository auftragsbuchRepository, JmsTemplate jmsTemplate, RestTemplate restTemplate) {
        return args -> {
            auftragsbuch = new Auftragsbuch(auftragsbuchRepository);
            auftragseingang = new Auftragseingang();
            auftragsausgang = new Auftragsausgang(jmsTemplate);

            Fahrzeug niceCar = new Fahrzeug("NICE-6969", Date.from(now()), AUTO);
            Fahrzeug balloonOfLove = new Fahrzeug("FLY-6969", Date.from(now()), HEISSLUFTBALLON);

            for (int i = 0; i < 200; i++) {
                //FahrzeugDto anderesFahrzeug = restTemplate.getForObject("http://10.132.1.60:9095/api/fahrzeuge/1", FahrzeugDto.class);
                //log.info("Schwachsinn legacy message: " + anderesFahrzeug + "");
                restTemplate.getForObject("http://localhost:9096/api/auftragsbuch", Object.class);
                Fahrzeug niceCar2 = new Fahrzeug("NICE-" + i, Date.from(now()), AUTO);
                ReparaturenTODOs reparaturenTODOs = new ReparaturenTODOs();
                reparaturenTODOs.add("Windschutzscheibe wechseln");
                niceCar2.reparaturVereinbaren(reparaturenTODOs);
                niceCar2.reparaturAbschliessen();
            }

            ReparaturenTODOs reparaturenTODOs = new ReparaturenTODOs();
            reparaturenTODOs.add("Windschutzscheibe wechseln");
            niceCar.reparaturVereinbaren(reparaturenTODOs);

            balloonOfLove.abschleppungBeauftragen(new Unfallort(79.7d, 55.5d));

            niceCar.reparaturAbschliessen();
            //balloonOfLove.abschleppungAbschliessen();

            FahrzeugDto anderesFahrzeug = restTemplate.getForObject("http://10.132.1.60:9095/api/fahrzeuge/1", FahrzeugDto.class);
            log.info("Schwachsinn legacy message: " + anderesFahrzeug + "");

        };
    }
}
