package de.andrena.rotz.werkstatt.domain;

import de.andrena.rotz.werkstatt.infrastructure.repository.AuftragsbuchRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class Auftragsbuch {

  @Autowired
  AuftragsbuchRepository auftragsbuchRepository;

  public void archiviere(Fahrzeug fahrzeug) {
    auftragsbuchRepository.delete(fahrzeug);
  }

  public void trageEin(Fahrzeug fahrzeug) {
    auftragsbuchRepository.save(fahrzeug);
  }

  public void finde(String kennzeichen) {
    auftragsbuchRepository.findByKennzeichen(kennzeichen);
  }

  public List<Fahrzeug> anzeige() {
    return auftragsbuchRepository.findAll();
  }
}
