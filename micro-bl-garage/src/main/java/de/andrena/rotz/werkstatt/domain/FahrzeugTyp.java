package de.andrena.rotz.werkstatt.domain;

public enum FahrzeugTyp {
    AUTO, JETSKI, ROLLER, HEISSLUFTBALLON
}
