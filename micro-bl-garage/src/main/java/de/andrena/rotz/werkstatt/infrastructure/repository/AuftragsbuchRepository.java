package de.andrena.rotz.werkstatt.infrastructure.repository;

import de.andrena.rotz.werkstatt.domain.Fahrzeug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuftragsbuchRepository extends JpaRepository<Fahrzeug, Integer> {

  Fahrzeug findByKennzeichen(String kennzeichen);

}
