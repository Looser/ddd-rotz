package de.andrena.rotz.werkstatt.domain;

import de.andrena.rotz.werkstatt.Werkstatt;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Fahrzeug extends Kopfdaten {

    private String kennzeichen;
    private Date tuevFaelligkeit;
    private FahrzeugTyp fahrzeugTyp;

    @Nullable
    @OneToOne(cascade = CascadeType.ALL)
    private Reparatur reparatur = null;
    @Nullable
    @OneToOne(cascade = CascadeType.ALL)
    private Abschleppung abschleppung = null;

    public Fahrzeug(String kennzeichen, Date tuevFaelligkeit, FahrzeugTyp typ) {
        this.kennzeichen = kennzeichen;
        this.tuevFaelligkeit = tuevFaelligkeit;
        this.fahrzeugTyp = typ;
    }

    public void reparaturVereinbaren(ReparaturenTODOs reparaturenTODOs) {
        reparatur = Reparatur.vereinbaren(reparaturenTODOs);
        Werkstatt.auftragsbuch.trageEin(this);
    }

    public void reparaturAbschliessen() {
        reparatur.ueberAbschlussInformieren(kennzeichen);
        Werkstatt.auftragsausgang.rechnungSenden("30");
        Werkstatt.auftragsbuch.archiviere(this);
    }

    public void abschleppungBeauftragen(Unfallort unfallort) {
        abschleppung = Abschleppung.beauftragen(unfallort);
        Werkstatt.auftragsbuch.trageEin(this);
    }

    public void abschleppungAbschliessen() {
        abschleppung.ueberAbschlussInformieren();
        Werkstatt.auftragsbuch.archiviere(this);
    }
}
