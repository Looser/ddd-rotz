package de.andrena.rotz.werkstatt.domain;

import de.andrena.rotz.werkstatt.Werkstatt;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Reparatur extends Kopfdaten {

    @Embedded
    private ReparaturenTODOs reparaturenTODOs;

    public static Reparatur vereinbaren(ReparaturenTODOs reparaturenTODOs) {
        return new Reparatur(reparaturenTODOs);
    }

    public void ueberAbschlussInformieren(String kennzeichen) {
        Werkstatt.auftragsausgang.abgeschlosseneReparaturMelden(kennzeichen);
    }
}
