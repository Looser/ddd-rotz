package de.andrena.rotz.werkstatt.domain;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Abschleppung extends Kopfdaten {

  @Embedded
  private Unfallort unfallort;

  public static Abschleppung beauftragen(Unfallort unfallort) {
    return new Abschleppung(unfallort);
  }

  public void ueberAbschlussInformieren() {
  }
}
