package de.andrena.rotz.microblmarketing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroBlMarketingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroBlMarketingApplication.class, args);
	}

}
