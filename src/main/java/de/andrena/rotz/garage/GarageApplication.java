package de.andrena.rotz.garage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GarageApplication {

	public static void main(String[] args) {
		//TEST
		SpringApplication.run(GarageApplication.class, args);
	}

}
